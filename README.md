# Ham Radio Scratchpad



## What is this?

These are some of my own notes on various radio projects. None of it will be organized particularly well. 

It might be helpful to someone, or helpful to me next time. :)

- [Xiegu X6100 - Armbian Notes](https://gitlab.com/csharpen/ham-radio-scratchpad/-/wikis/Xiegu-X6100-Armbian)
- [Set Windows default audio mic and speakers](https://gitlab.com/csharpen/ham-radio-scratchpad/-/blob/main/SetAudioDevice.ps1)
