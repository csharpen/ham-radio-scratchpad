param ($RequestedAction)
# A non-elegant but simple script to change the default audio devices for Mic/Recording/Communications, and Playback
# If you don't know the devices you're looking for, hit powershell and do Get-AudioDevice -List or open mmsys.cpl

# Using: https://github.com/frgnca/AudioDeviceCmdlets
#   Install-Module -Name AudioDeviceCmdlets
# Run with PowerShell -ExecutionPolicy Bypass
# Use a shortcut like: powershell.exe -execution bypass -nologo -file  C:\scripts\Audio\SetAudioDevices.ps1 3

# Get a current device list (doing this just once is a little faster)
$AudioDevices = (Get-AudioDevice -List)

# Find the devices you want to work with and toss them into variables. Not required, but simple.
$LaptopSpeaker = ($AudioDevices |where Name -like 'Speakers*Realtek*Audio*')
$LaptopOnboardMic = ($AudioDevices |where Name -like 'Microphone *MSI Sound Tune*')
$HeadphoneSpeakerJack = ($AudioDevices |where Name -like '*Realtek HD Audio 2nd output*')
$HeadphoneMicJack = ($AudioDevices |where Name -like '*Microphone*Realtek*Audio*')

# Based on the command-line arg, decide which set of actions to perform
switch($RequestedAction) {
  1 {   Write-Host "Setting audio to: Laptop Speaker and Headphone Mic (Jack)" | out-host
        Set-AudioDevice	$LaptopSpeaker.ID
        Set-AudioDevice	$HeadphoneMicJack.ID
    }

  2 {   Write-Host "Setting audio to: Laptop Speaker and Laptop Mic (built-in)" | out-host
        Set-AudioDevice	$LaptopSpeaker.ID
        Set-AudioDevice	$LaptopOnboardMic.ID
    }

  3 {   Write-Host "Setting audio to: Headphone Speaker and Headphone Mic" | out-host
        Set-AudioDevice	$HeadphoneSpeakerJack.ID
        Set-AudioDevice	$HeadphoneMicJack.ID
    }

  4 {   Write-Host "Setting audio to: Headphone Speaker and Laptop Mic" | out-host
        Set-AudioDevice	$HeadphoneSpeakerJack.ID
        Set-AudioDevice	$LaptopOnboardMic.ID
    }

    default {write-warning "Error - incorrect or missing requested action. What should I do?"}
}
